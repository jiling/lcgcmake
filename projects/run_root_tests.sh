#!/bin/bash

# Run Root tests from the build directory
# run-root-tests.sh <local-prefix>  <platform>  <build-dir>

PREFIX=$1
PLATFORM=$2
BINARY_DIR=$3

# where is lcgenv?
if [ -z "$LCGENV" ]; then
  if command -v lcgenv &>/dev/null; then
     LCGENV=`command -v lcgenv`
  elif [ -x /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv ]; then
     LCGENV=/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv
  fi
fi

# how many cores?
CPUS=`grep -c ^processor /proc/cpuinfo`

# set current directory and environment
cd ${BINARY_DIR}

IGNORE="test-periodic-build|test-import-pandas|pyunittests-pyroot-dependency-versions|tutorial-analysis-dataframe-df026_AsNumpyArrays-py|tutorial-roofit-rf409_NumPyPandasToRooFit-py|tutorial-analysis-dataframe-df035_RDFFromPandas-py|tutorial-roofit-rf615_simulation_based_inference-py|rf617_simulation_based_inference_multidimensional|tutorial-roofit-rf618_mixture_models-py|test-import-torch|test-import-tensorflow|gtest-bindings-tpython-testTPython|pyunittests-bindings-pyroot-pythonizations-batchgen|tutorial-dataframe-df026_AsNumpyArrays-py|tutorial-dataframe-df035_RDFFromPandas-py"

# tutorial-analysis-dataframe-df026_AsNumpyArrays-py needs pandas
# tutorial-dataframe-df026_AsNumpyArrays-py needs pandas, old name of the test
# tutorial-analysis-dataframe-df035_RDFFromPandas-py needs pandas
# tutorial-dataframe-df035_RDFFromPandas-py needs pandas, old name of the test
# tutorial-roofit-rf409_NumPyPandasToRooFit-py needs pandas
# test-import-pandas needs pandas
# pyunittests-pyroot-dependency-versions needs some more python packages that are a pain to provide without re-arranging too many recipes
# tutorial-roofit-rf615_simulation_based_inference-py needs scikitlearn that is  a pain to re-arrange
# rf617_simulation_based_inference_multidimensional
# tutorial-roofit-rf618_mixture_models-py needs xgboost
# test-import-torch needs torch
# test-import-tensorflow needs tensorflow
# gtest-bindings-tpython-testTPython
# pyunittests-bindings-pyroot-pythonizations-batchgen needs torch and tensorflow
ctest --version
ctest -j${CPUS} --output-on-failure -E ${IGNORE}
