set(MCGENPATH  MCGenerators)

# HEPMC_VERSION allows to switch between HepMC2 and HepMC3 as default HepMC library (i.e. 2 or 3)
if(NOT HEPMC_VERSION)
  set(HEPMC_VERSION 3)
endif()

if(NOT "${LCG_COMP}" STREQUAL "clang" AND NOT "${LCG_OS}" MATCHES "ubuntu")
  LCG_external_package(nlox 1.2.2.atlas5 ${MCGENPATH}/nlox)
endif()

LCG_external_package(geneva            1.0-rc3.atlas2 ${MCGENPATH}/geneva author=1.0-rc3 )
LCG_external_package(pepper_kokkos     1.1.1          ${MCGENPATH}/pepper_kokkos )

LCG_external_package(SFGen             1.03.atlas2    ${MCGENPATH}/SFGen author=1.03)
LCG_external_package(apfel             3.1.0          ${MCGENPATH}/apfel )

LCG_external_package(contur            3.0.0      ${MCGENPATH}/contur)

LCG_external_package(sherpa            3.0.1p1        ${MCGENPATH}/sherpa   hepmc=${HEPMC_VERSION}   author=3.0.1 hepevt=200000)
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu20)  # No MPI support for ubuntu20 native compiler
  LCG_external_package(sherpa-openmpi  3.0.1p1.openmpi3  ${MCGENPATH}/sherpa hepmc=${HEPMC_VERSION}  author=3.0.1 hepevt=200000)
endif()

LCG_external_package(openloops         2.1.3p2        ${MCGENPATH}/openloops)

LCG_external_package(thepeg            2.3.0          ${MCGENPATH}/thepeg hepmc=${HEPMC_VERSION})
LCG_external_package(herwig3           7.3.0p1        ${MCGENPATH}/herwig++ thepeg=2.3.0 hepmc=${HEPMC_VERSION} author=7.3.0)

LCG_external_package(yoda              2.0.2          ${MCGENPATH}/yoda  )
LCG_external_package(rivet             4.0.2          ${MCGENPATH}/rivet hepmc=${HEPMC_VERSION} author=4.0.2)

LCG_external_package(heputils          1.4.0          ${MCGENPATH}/heputils )

LCG_external_package(mcutils           1.4.0          ${MCGENPATH}/mcutils)

if(NOT ${LCG_COMP} STREQUAL clang) 
  LCG_external_package(mcfm              10.3.atlas   ${MCGENPATH}/mcfm author=10.3)
  # ggvvamp and qqvvamp removed for clang builds as -I to gcc header quadmath.h is not passed in clang wrapper 
  LCG_external_package(ggvvamp           1.0.atlas1     ${MCGENPATH}/ggvvamp)
  LCG_external_package(qqvvamp           1.1.atlas1     ${MCGENPATH}/qqvvamp)  
endif()

LCG_external_package(collier           1.2.8          ${MCGENPATH}/collier)
LCG_external_package(syscalc           1.1.7          ${MCGENPATH}/syscalc)

LCG_external_package(madgraph5amc      3.5.7.atlas11  ${MCGENPATH}/madgraph5amc author=3.5.7)

LCG_external_package(lhapdf            6.5.5          ${MCGENPATH}/lhapdf       )

if(NOT(${LCG_COMP} STREQUAL clang) AND NOT(${LCG_OS}${LCG_OSVERS} MATCHES ubuntu))
LCG_external_package(powheg-box-v2     r3744.lhcb3.rdynamic    ${MCGENPATH}/powheg-box-v2 )
endif()

LCG_external_package(feynhiggs         2.10.2         ${MCGENPATH}/feynhiggs	)
LCG_external_package(chaplin           1.2            ${MCGENPATH}/chaplin      )

LCG_external_package(pythia8           313            ${MCGENPATH}/pythia8  author=313)

LCG_external_package(looptools         2.15           ${MCGENPATH}/looptools)

LCG_external_package(vbfnlo            3.0            ${MCGENPATH}/vbfnlo hepmc=${HEPMC_VERSION})
LCG_external_package(FORM              4.3.1          ${MCGENPATH}/FORM)

LCG_external_package(njet              2.1.1          ${MCGENPATH}/njet)
LCG_external_package(qgraf             3.1.4          ${MCGENPATH}/qgraf)
LCG_external_package(gosam_contrib     2.0            ${MCGENPATH}/gosam_contrib)
LCG_external_package(gosam             2.1.2          ${MCGENPATH}/gosam)

LCG_external_package(tauola++          1.1.8.atlas1   ${MCGENPATH}/tauola++ author=1.1.8 hepmc=${HEPMC_VERSION})

LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=200000  )

LCG_external_package(photos++          3.64           ${MCGENPATH}/photos++ hepmc=${HEPMC_VERSION})

LCG_external_package(evtgen            2.2.1          ${MCGENPATH}/evtgen       tag=R02-02-01 hepmc=${HEPMC_VERSION})

LCG_external_package(hijing            1.383bs.2      ${MCGENPATH}/hijing       )

LCG_external_package(starlight         r330           ${MCGENPATH}/starlight   )

LCG_external_package(qd                2.3.13         ${MCGENPATH}/qd          )

LCG_external_package(photos            215.4          ${MCGENPATH}/photos       )
if( NOT ${LCG_COMP} STREQUAL clang ) # Needs CLHEP
LCG_external_package(hepmcanalysis     3.4.14         ${MCGENPATH}/hepmcanalysis  author=00-03-04-14 )
endif()
LCG_external_package(mctester          1.25.1         ${MCGENPATH}/mctester hepmc=${HEPMC_VERSION} )

LCG_external_package(herwig            6.521.2        ${MCGENPATH}/herwig       )

LCG_external_package(crmc              2.0.1p5        ${MCGENPATH}/crmc  author=2.0.1 hepmc=${HEPMC_VERSION}   )

LCG_external_package(hydjet            1.8            ${MCGENPATH}/hydjet author=1_8 )
LCG_external_package(tauola            28.121.2       ${MCGENPATH}/tauola       )

LCG_external_package(jimmy             4.31.3         ${MCGENPATH}/jimmy        )
LCG_external_package(hydjet++          2.1            ${MCGENPATH}/hydjet++ author=2_1)
LCG_external_package(alpgen            2.1.4          ${MCGENPATH}/alpgen author=214 )
LCG_external_package(pyquen            1.5.1          ${MCGENPATH}/pyquen author=1_5)
LCG_external_package(baurmc            1.0            ${MCGENPATH}/baurmc       )

LCG_external_package(professor         2.4.1          ${MCGENPATH}/professor    )

LCG_external_package(jhu               5.6.3          ${MCGENPATH}/jhu          )

#LCG_external_package(log4cpp           2.9.1          ${MCGENPATH}/log4cpp     )

LCG_external_package(rapidsim          1.4.4          ${MCGENPATH}/rapidsim     )

LCG_external_package(superchic         5.4            ${MCGENPATH}/superchic   author=5.4 )
LCG_external_package(cepgen            1.2.5patch2    ${MCGENPATH}/cepgen        )

if(NOT ${LCG_OS} MATCHES ubuntu|mac)
LCG_external_package(hoppet              1.2.0          ${MCGENPATH}/hoppet        )
endif()
if(NOT ${LCG_OS} MATCHES mac)
LCG_external_package(hto4l               2.02           ${MCGENPATH}/hto4l         )
LCG_external_package(prophecy4f          3.0.2          ${MCGENPATH}/prophecy4f    )
endif()

LCG_external_package(ampt                2.26t9b_atlas  ${MCGENPATH}/ampt author=2.26t9b )

LCG_external_package(recola_SM           2.2.3          ${MCGENPATH}/recola_SM      )
LCG_external_package(recola              2.2.4          ${MCGENPATH}/recola         )
if(${LCG_COMP} STREQUAL gcc AND (${LCG_COMPVERS} VERSION_LESS 9 OR ${LCG_COMPVERS} VERSION_EQUAL 62))
LCG_external_package(hjets               1.2  ${MCGENPATH}/hjets author=1.2)
# error: ‘const class std::complex<ThePEG::Qty<std::ratio<0>, std::ratio<0>, std::ratio<0> > >’ has no member named ‘__rep’
endif()

LCG_external_package(thep8i              2.0.3         ${MCGENPATH}/thep8i  )

if(${LCG_OS} MATCHES mac)
  LCG_remove_package(sherpa-openmpi)
endif()


