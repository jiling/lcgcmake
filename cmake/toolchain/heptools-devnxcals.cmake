#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

LCG_external_package(ROOT v6-32-00-patches GIT=http://root.cern.ch/git/root.git)

#---Finding latest versions of nxcals packages---------------------
#Initialize token for eos access:
execute_process(COMMAND ${CMAKE_SOURCE_DIR}/jenkins/kinit.sh)

set(EOS_PATH_NXCALS_DEV /eos/project/n/nxcals/swan/nxcals_testbed)
set(NXCALS_LS_COMMAND "xrdfs root://eosproject.cern.ch ls ")
set(NXCALS_CP_COMMAND "xrdcp")
set(NXCALS_MGM_URL root://eosproject.cern.ch://)
set(PATTERN_NXCALS_JAVA "data-access-libs")
set(PATTERN_NXCALS_PYTHON "extraction_api_python3-")

# Allow download of the beta version
set(BETA_PIP_FLAG "--pre")
# limit pytimber to version 4 (new version)
set(pytimber_VERSION_LIMIT "~=4.0")

# a number followed by repeated (a dot followed by numbers or rc or b or (dev0) or letters or both)
set(NXCALS_VERSION_MATCH "[0-9]((\\.|rc|b|)((dev)?[0-9]+)+)+")

# upgrade pip for latest version
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip install --upgrade --user pip")

execute_process(COMMAND $ENV{SHELL} -c "${NXCALS_LS_COMMAND} ${EOS_PATH_NXCALS_DEV}" OUTPUT_VARIABLE NXCALS_CONTENT OUTPUT_STRIP_TRAILING_WHITESPACE)

execute_process(COMMAND $ENV{SHELL} -c "${NXCALS_LS_COMMAND} ${EOS_PATH_NXCALS_DEV} | grep -v '\.sys\.' | grep ${PATTERN_NXCALS_JAVA}   | grep -Eo \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE NXCALS_JAVA_VERSION   OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "${NXCALS_LS_COMMAND} ${EOS_PATH_NXCALS_DEV} | grep -v '\.sys\.' | grep ${PATTERN_NXCALS_PYTHON} | grep -Eo \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE NXCALS_PYTHON_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

MESSAGE(STATUS "Content of NXCals folder is\n********\n${NXCALS_CONTENT}\n********")
MESSAGE(STATUS "And we grepped the versions\n********\ndata-access-libs ${NXCALS_JAVA_VERSION}\nextraction-api: ${NXCALS_PYTHON_VERSION}\n********")

# get the python major.minor from the stack
string(REGEX MATCH "[0-9]*.[0-9]*" NXCALS_PYTHON_DIGITS ${Python_native_version})

# we need the newer pip for these options
set(NXCALS_PIP_FLAGS "--ignore-requires-python --python-version ${NXCALS_PYTHON_DIGITS}")

#---Finding latest versions of acc-py packages---------------------
foreach(ACC_PACKAGE pytimber cmw_tracing nxcals_py4j_utils nxcals_type_stubs nxcals)
  execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download ${NXCALS_PIP_FLAGS} ${BETA_PIP_FLAG} --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d ${CMAKE_BINARY_DIR}/acc-py-cache --no-deps ${ACC_PACKAGE}${${ACC_PACKAGE}_VERSION_LIMIT}")
  execute_process(COMMAND $ENV{SHELL} -c "ls -1t ${CMAKE_BINARY_DIR}/acc-py-cache | grep ${ACC_PACKAGE} | head -n1 | grep -Eo  \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE ${ACC_PACKAGE}_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
  MESSAGE(STATUS "We found package ${ACC_PACKAGE} with version  ${${ACC_PACKAGE}_VERSION}")
  LCG_external_package(${ACC_PACKAGE} ${${ACC_PACKAGE}_VERSION})
endforeach()

#---Nxcals needs Java11--------------------------------------------
LCG_external_package(java                     11.0.11_9                     )

#---Add nxcals packages--------------------------------------------
LCG_external_package(nxcals_data_access_libs  "${NXCALS_JAVA_VERSION}"      )
LCG_external_package(nxcals_extraction_api    "${NXCALS_PYTHON_VERSION}"    )

#---Remove packages not needed by nxcals--------------
LCG_remove_package(Garfield++)
