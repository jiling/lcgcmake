#!/usr/bin/python3
from __future__ import print_function
import argparse
import os
import re
import subprocess
import shlex
from pathlib import Path
import itertools
from time import sleep

from common_parameters import tmparea, archivearea, updatesarea, eosmgmurl

def getRevNum(a):
    """RPM revision number from the name"""
    aa=a.split('-')
    if len(aa) > 1 :
        bb=aa[-1].split('.')
        if bb[0].isdigit() :
            return int(bb[0])
    # Not a digit
    return -1

def get_list_to_copy(new, existing, newrpms):
    """Get the list of RPM to copy"""

    # All to copy
    if not existing:
        for f in new:
            newrpms.append(f)
        return len(newrpms)

    # Find the new ones with highest revision number
    for f in new:
        _fb = os.path.basename(f)
        _aa = _fb.split('-')
        if len(_aa) > 1:
            _fn = _aa[0] + '-' + _aa[1]
            _rn = getRevNum(_fb)
            _found = False
            for fe in existing:
                _fe=str(fe.name)
                if _fe.startswith(_fb):
                    _found = True
                    _rne = getRevNum(_fe)
                    if _rn > _rne:
                        print(_fb, ': new revision ', _rn, ' superseeding old one (', _rne, ')')
                        newrpms.append(f)
                    break
            if not _found:
                print(_fb,' compatible not found')
                newrpms.append(f)
        else:
            print('file: |', _fb, '| is not standard ')
    # return output length
    return len(newrpms)

def copy_to_eos(from_mask, to, dryrun):
    """Copy files to EOS, retrying multiple times in case of network glitches"""

    max_attempts = 5
    timeout_seconds = 1
    # from_ = from_.replace('project/l', 'project-l')
    for from_file in Path('/').glob(from_mask.lstrip('/')):
        print("Copy", from_file, "to", to, end=' ')
        if dryrun:
            print("\t[DRYRUN]")
        else:
            command = "xrdcp -s %s %s" % (from_file, to)

            for attempt in range(1, max_attempts + 1):
                try:
                    subprocess.check_output(shlex.split(command), stderr=subprocess.STDOUT)
                except subprocess.CalledProcessError as e:
                    eossrv = to.rsplit('//', 1)[0]
                    eosfnm = '/' + to.rsplit('//', 1)[1] + os.path.basename(from_file)
                    rc = subprocess.run(['xrdfs', eossrv, 'ls', '-1', eosfnm])
                    if rc.returncode == 0:
                        print("\t[EXISTS]")
                        break
                    else:
                        print("Attempt {0} of {1} failed!".format(attempt, max_attempts))
                        print("\t[ERROR {0}]".format(e.returncode))
                        print(e.output.decode("utf-8"))
                else:
                    print("\t[OK]")
                    break
                sleep(timeout_seconds)
                timeout_seconds *= 2

def main(args):
    """Main steering function making sure that the RPMs are copied in the right place"""

    p = re.compile("^[0-9]*")
    version = args.version
    version_main = p.match(version).group()
    platform = args.platform
    dryrun = args.dryrun

    list_existing_metarpms = []
    list_existing_packrpms = []

# -----------
# New RPM structure (March 2022) is of the form:
#   <RPM-root>/<OS-num>/<barearch>[/debug]/Packages/
#   <RPM-root>/<OS-num>/<barearch>[/debug]/LCG_<LCG-num>/LCG_<LCG-num>[<LGC-layer-label>]/
# e.g.
#   /eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/debug/Packages/
#   /eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/debug/LCG_101/LCG_101_LHCB_7/
# -----------

    ac = platform.split('_')
    arch = '_'.join(ac[:-2])
    barearch = '_'.join(ac[:-3])
    btyp = '/debug' if ac[-1] == 'dbg' else ''
    rpmsarea = args.repo.rstrip('/') + '/' + barearch + btyp
    targetarea = rpmsarea + '/LCG_' + str(version_main) + '/' + 'LCG_' + version
    packagesarea = rpmsarea + '/Packages'
    list_existing_metarpms = list(itertools.filterfalse(
        lambda r: False,
        itertools.chain(
            Path(targetarea).glob('*{0}*.rpm'.format(platform))
        ))
    )
    list_existing_packrpms = list(itertools.filterfalse(
        lambda r: False,
        itertools.chain(
            Path(packagesarea).glob('*{0}*.rpm'.format(platform))
        ))
    )

    print("Copy RPMs from {0}/LCG_{1} to {2} and {3}".format(tmparea, version, targetarea, packagesarea ))

    list_new_metarpms = []
    list_new_packrpms = []

    print(f"Collected {len(list_existing_metarpms)} meta RPMs paths from ", targetarea)
    print(f"Collected {len(list_existing_packrpms)} package RPMs paths from ", packagesarea)

    for f in itertools.chain(
        (Path(tmparea) / f"LCG_{version}").glob(f'[!.]*{platform}*.rpm'),
        (Path(tmparea) / f"LCG_{version}").glob(f'[!.]*{arch}-*.rpm')) :
        if f.is_dir():
            continue
        if f.name.startswith((f'LCG_{version}', f'LCGMeta_{version}')):
            list_new_metarpms.append(str(f))
        if not f.name.startswith('LCG'):
            list_new_packrpms.append(str(f))

    print(f"Collected {len(list_new_metarpms)}|{len(list_new_packrpms)} meta|package RPMs paths from ", str(Path(tmparea) / f"LCG_{version}"))

    list_to_copy_metarpms = []
    n_metarpms = get_list_to_copy(list_new_metarpms, list_existing_metarpms, list_to_copy_metarpms)
    list_to_copy_packrpms = []
    n_packrpms = get_list_to_copy(list_new_packrpms, list_existing_packrpms, list_to_copy_packrpms)

    print(f"Will copy {len(list_to_copy_metarpms)}|{len(list_to_copy_packrpms)} new meta|package RPMs ")

    target = {True: f"{targetarea}/", False: f"{packagesarea}/"}

    for d in target.values():
        if not args.dryrun:
            os.makedirs(d, exist_ok=True)

    for t in list_to_copy_metarpms:
        copy_to_eos(str(t), eosmgmurl + target[True], dryrun)
    for t in list_to_copy_packrpms:
        copy_to_eos(str(t), eosmgmurl + target[False], dryrun)

def main_bc(args):
    """Steering function provided for backward compatibility, making sure that the RPMs are copied
       in the right place for LCG versions prior to 102"""

    version = args.version
    platform = args.platform
    dryrun = args.dryrun
    if args.legacy:
        existing_rpms = itertools.filterfalse(
            lambda r: r.name.startswith('LCG'),
            itertools.chain(
                Path(archivearea).glob('*{0}*.rpm'.format(platform)),
                Path(updatesarea).glob('*{0}*.rpm'.format(platform))
            )
        )
        targetarea = updatesarea
        platform = '_'
        arch = '_'
    else:
        targetarea = args.repo.rstrip('/')
        existing_rpms = itertools.filterfalse(
            lambda r: r.name.startswith('LCG'),
            itertools.chain(
                Path(archivearea).glob('*{0}*.rpm'.format(platform)),
                Path(updatesarea).glob('*{0}*.rpm'.format(platform)),
                Path(targetarea).glob('*{0}*.rpm'.format(platform)),
            )
        )
        ac = platform.split('_')
        arch = '_'.join(ac[:-2])

    print("Copy RPMs from {0}/LCG_{1} to {2}, archive area {3}".format(tmparea, version, targetarea, archivearea))

    lcg_subdirectory = "LCG_" + version
    if args.legacy:
        lcg_subdirectory += "release"

    # lcg_directory = targetarea + lcg_subdirectory
    # os.environ['EOS_MGM_URL'] = eosmgmurl

    list_existing = set()
    list_new = set()

    # os.makedirs(lcg_directory, exist_ok=True)

    print("Collecting existing prefixes...")
    for j in existing_rpms:
        if j.is_dir():
            continue

        rpm_prefix = j.name.split("-1.0.0")[0]
        list_existing.add(rpm_prefix)

    print(f"Collected {len(list_existing)} prefixes")

    print("Collecting incoming prefixes from", str(Path(tmparea) / f"LCG_{version}"))
    for f in itertools.chain(
        (Path(tmparea) / f"LCG_{version}").glob(f'[!.]*{platform}*.rpm'),
        (Path(tmparea) / f"LCG_{version}").glob(f'[!.]*{arch}*.rpm')
    ):
        if f.is_dir():
            continue

        if f.name.startswith(f'LCG_{version}') or not f.name.startswith('LCG_'):
            rpm_prefix = f.name.split("-1.0.0")[0]
            list_new.add(rpm_prefix)

    print(f"Collected {len(list_new)} prefixes")

    list_to_copy = list_new.difference(list_existing)

    print(f"Will copy {len(list_to_copy)} new prefixes")

    target = {True: f"{targetarea}/{lcg_subdirectory}/", False: f"{targetarea}/"}
    if not args.legacy:
        target[False] = f"{targetarea}/Packages/"

    for d in target.values():
        if not args.dryrun:
            os.makedirs(d, exist_ok=True)

    for t in list_to_copy:
        copy_to_eos(str(Path(tmparea) / f"LCG_{version}" / f'{t}*.rpm'), eosmgmurl + target[t.startswith('LCG')], dryrun)


if __name__ == "__main__":
    """Main scope where the right main steering function is called"""

    parser = argparse.ArgumentParser()
    parser.add_argument("version", help="LCG release version, e.g. 88 ot 97python3")
    parser.add_argument("repo", help="Path to target RPM repository, ignored in legacy mode")
    parser.add_argument("platform", help="Platform, ignored in legacy mode")
    parser.add_argument("-n", "--dryrun", default=False, action="store_true")
    parser.add_argument("--legacy", default=False, action="store_true", help="Legacy mode: for LCG_96 and older")
    parser.add_argument("--legacy101", default=False, action="store_true", help="Internediate mode: for LCG_97-LCG_101")
    arguments = parser.parse_args()

    if arguments.legacy101 or arguments.legacy:
        # LCG_101 and older
        main_bc(arguments)
    else:
        main(arguments)
