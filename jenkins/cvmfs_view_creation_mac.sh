#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

REPOSITORY='sft-nightlies.cern.ch'
weekday=`date +%a`
cvmfs_user=cvsft-nightlies
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch

cvmfs_server transaction -t 60 ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
retVal=\$?
echo "retVal: \$retVal"
if [[ "\$retVal" == "17" ]]; then
  # return value 17 is transaction in progress (EEXIST)
  echo "There is an open transaction that shouldn't exist... Forcing transaction abortion"
  set +e
  # try to abort the transaction 5 times or until it is successful
  for iterations in {1..5}
  do
    cd /home
    cvmfs_server abort -f ${REPOSITORY}
    if [ "\$?" == "0" ] || [ "\$?" == "22" ]; then
      break
    else
      echo "Error aborting transaction. Going to try again in 30 seconds..."
      sleep 30
    fi
    if [[ "\$iterations" == "5" ]]; then
      echo "There is an error aborting the transaction... "
      exit 1
    fi
  done
  set -e
  # if the transaction is successfully aborted, start a new one
  cvmfs_server transaction ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
elif [ "\$retVal" != "0" ]; then
  echo "Error starting transaction"
  # try to abort the transaction, if that doesnt work, exit
  cd /home
  cvmfs_server abort -f
  if [ "\$?" != "0" ]; then
    echo "Error aborting transaction. Exiting..."
    exit 1
  fi
  # if the transaction is successfully aborted, start a new one
  cvmfs_server transaction ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
  # if the transaction is not successfully started, exit
  if [ "\$?" != "0" ]; then
    echo "Error starting transaction. Exiting..."
    exit 1
  fi
fi

cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" --del sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}/* /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}

if [ "\$?" == "0" ]; then
    cd /home
    cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
    echo "View created, updating latest symlink"

    # timeout because these transactions would block each other
    cvmfs_server transaction -t 3000 ${REPOSITORY}/lcg/views/${LCG_VERSION}/latest

    rm -f /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/latest/${PLATFORM}
    cd /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/latest
    ln -s ../${weekday}/${PLATFORM}

    cd /home
    cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/latest
else
    echo "There was an error copying the view, aborting"
    cvmfs_server abort -f ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
    exit 1
fi
EOF
