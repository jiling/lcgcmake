#--- General parameters ----------------------------------------------------------------------------
set(Python_cmd ${Python_home}/bin/${PYTHON})
set(PySetupOptions --root=/ --prefix=<INSTALL_DIR>)

#---Gaudi----------------------------------------------------------------------------
LCGPackage_Add(
  Gaudi
  URL ${GenURL}/Gaudi-<VERSION>.tar.gz
  UPDATE_COMMAND <VOID>
  ENVIRONMENT BINARY_TAG=${LCG_platform}
              ROOT_INCLUDE_PATH=${Vc_home}/include  # Needed at build time in dbg for dictionaries
              PYTHONPATH=${ROOT_home}/lib  # Needed at runtime for tests
  CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR> 
             -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             IF ${CMAKE_BUILD_TYPE} STREQUAL Debug AND ${LCG_TARGET} MATCHES "clang" THEN 
               -DGAUDI_SLOW_DEBUG=TRUE
             ENDIF
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
             -DCMAKE_FIND_FRAMEWORK=LAST 
             -DBoost_NO_BOOST_CMAKE=FALSE
             -DHOST_BINARY_TAG=${LCG_platform}
	     IF ${LCG_OS} MATCHES ubuntu THEN
	        -DGAUDI_USE_GPERFTOOLS=FALSE
	     ENDIF
  INSTALL_COMMAND ${MAKE} install
          IF NOT <VERSION> STREQUAL master AND <VERSION> VERSION_LESS v35r0 THEN
             COMMAND ${EXEC} bash -c "cp -n -r <INSTALL_DIR>/*.cmake <INSTALL_DIR>/cmake"
          ENDIF
  DEPENDS Boost Python GSL ROOT clhep AIDA XercesC rangev3 cppgsl xenv six fmt pytest PyYAML
          jsonmcpp HepPDT CppUnit libunwind doxygen Catch2 networkx pytest_cov jemalloc
          IF NOT ${LCG_OS} MATCHES ubuntu THEN gperftools ENDIF
)

#   Add Gaudi tests only of Gaudi is actually build (disable ubuntu for the time being)
if(TARGET Gaudi-${Gaudi_native_version} AND NOT ${LCG_OS} MATCHES ubuntu)
  get_property(gaudi-binary-dir TARGET Gaudi-${Gaudi_native_version} PROPERTY _EP_BINARY_DIR)
  if( gaudi-binary-dir )
    LCG_add_test(gaudi-tests
      TEST_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/run_gaudi_tests.sh ${CMAKE_INSTALL_PREFIX} ${LCG_platform} ${gaudi-binary-dir}
      LABELS Nightly Release Experimental
      ENVIRONMENT ROOT_INCLUDE_PATH=${Vc_home}/include  # Needed at runtime for tests
                  PYTHONPATH=${ROOT_home}/lib:${pytest_cov_home}/lib/python${Python_config_version_twodigit}/site-packages
    )
  endif()
endif()

#---PODIO-------------------------------------------------------------------------------------------
LCGPackage_add(
    podio
    URL ${GenURL}/podio-${podio_native_version}.tar.gz
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
               -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
               -DBUILD_TESTING=OFF
               IF podio_native_version VERSION_GREATER 00.12 AND SIO_native_version THEN
               -DENABLE_SIO=ON
               ENDIF
               IF <VERSION> VERSION_GREATER 1.0 THEN
               -DENABLE_RNTUPLE=ON
               -DENABLE_DATASOURCE=ON
               ENDIF
    DEPENDS ROOT Python PyYAML
    IF podio_native_version VERSION_GREATER 1.0 THEN
       tabulate graphviz
    ENDIF
    IF podio_native_version VERSION_GREATER 00.12 THEN
    DEPENDS_OPT SIO
    ENDIF
    REVISION 1
)

#---EDM4hep---------------------------------------------------------------------
LCGPackage_add(
    EDM4hep
    URL ${GenURL}/EDM4hep-${EDM4hep_native_version}.tar.gz
    ENVIRONMENT CPLUS_INCLUDE_PATH=${vdt_home}/include:$ENV{CPLUS_INCLUDE_PATH}
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
               -DBUILD_TESTING=OFF 
    # Remove after EDM4hep 0.99.1
    PATCH_COMMAND $ENV{SHELL} -c "echo 'find_dependency(nlohmann_json)' >> <SOURCE_DIR>/cmake/EDM4HEPConfig.cmake.in"
    BUILD_COMMAND ${MAKE}
    DEPENDS     ROOT podio Jinja2 jsonmcpp
)

#---DD4hep---------------------------------------------------------------------
if(NOT DEFINED DD4hep_${DD4hep_native_version}_geant4)
   set(DD4hep_${DD4hep_native_version}_geant4 "ON")
endif()

LCGPackage_add(
    DD4hep
    URL ${GenURL}/DD4hep-${DD4hep_native_version}.tar.gz
    IF LCG_TARGET MATCHES mac AND <VERSION> VERSION_EQUAL 01.28 THEN
      PATCH_COMMAND patch -l -p0 -b -i ${CMAKE_CURRENT_SOURCE_DIR}/patches/DD4hep-01.28-macos.patch
    ENDIF
    IF DEFINED Vc_native_version THEN
        ENVIRONMENT ROOT_INCLUDE_PATH=${Vc_home}/include
    ENDIF
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DBOOST_ROOT=${Boost_home}
               -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
               -DDD4HEP_USE_XERCESC=ON
               -DXERCESC_ROOT_DIR=${XercesC_home}
               -DROOTSYS=${ROOT_home}
               IF <DD4hep_<NATIVE_VERSION>_geant4> STREQUAL "ON" THEN
                  -DDD4HEP_USE_GEANT4=ON
               ENDIF
               IF hepmc3_native_version THEN
                  -DDD4HEP_USE_HEPMC3=ON
               ENDIF
               IF EDM4hep_native_version THEN
                  -DDD4HEP_USE_EDM4HEP=ON
               ENDIF
               -DBUILD_DOCS=OFF
               -DDD4HEP_USE_LCIO=ON
	       -DDD4HEP_USE_TBB=ON
               -DDD4HEP_LOAD_ASSIMP=OFF
               -DDD4HEP_BUILD_EXAMPLES=OFF
               ${Boost_extra_configuration}
    BUILD_COMMAND ${MAKE} -j4 -k
    DEPENDS     ROOT XercesC Boost Python pytest LCIO tbb IF <DD4hep_<NATIVE_VERSION>_geant4> STREQUAL "ON" THEN Geant4 ENDIF
    DEPENDS_OPT assimp hepmc3 EDM4hep
    REVISION 2
)

if(TARGET DD4hep-${DD4hep_native_version} AND LCG_OS MATCHES el)
  get_property(dd4hep-binary-dir TARGET DD4hep-${DD4hep_native_version} PROPERTY _EP_BINARY_DIR)
  if( dd4hep-binary-dir )
    LCG_add_test(dd4hep-tests
      TEST_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/run_dd4hep_tests.sh ${CMAKE_INSTALL_PREFIX} ${LCG_platform} ${dd4hep-binary-dir} ${DD4hep_native_version}
      LABELS Nightly Release Experimental
      ENVIRONMENT ROOT_INCLUDE_PATH=${Vc_home}/include  # Needed at runtime for tests
      TIMEOUT 3600
    )

    LCG_add_test(dd4hep-example-tests
      TEST_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/run_dd4hep_example_tests.sh ${CMAKE_INSTALL_PREFIX} ${LCG_platform} ${CMAKE_CURRENT_BINARY_DIR}/DD4hep-${DD4hep_native_version}/src/DD4hep/${DD4hep_native_version} ${DD4hep_native_version}
      LABELS Nightly Release Experimental
      ENVIRONMENT ROOT_INCLUDE_PATH=${Vc_home}/include
                  "CMAKE_PREFIX_PATH=${XercesC_home}:${jsonmcpp_home}:${vdt_home}:${tbb_home}:${Boost_home}"
                  "CMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}"
      TIMEOUT 3600
    )

  endif()
endif()

#--- Acts_core --------------------------------------------------------------------------------------
# Kept for legacy
LCGPackage_add(
    acts_core
    URL ${GenURL}/acts-core-${acts_core_native_version}.tar.gz
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DACTS_BUILD_IDENTIFICATION_PLUGIN=ON
               -DACTS_BUILD_DIGITIZATION_PLUGIN=ON
               -DACTS_BUILD_DD4HEP_PLUGIN=ON
               -DACTS_BUILD_TGEO_PLUGIN=ON
               -DACTS_BUILD_LEGACY=ON
               -DACTS_BUILD_TESTS=OFF
               -DEIGEN_INCLUDE_DIR=${eigen_home}/include/eigen3
               "-DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -I${XercesC_home}/include"
               ${Boost_extra_configuration}
    DEPENDS Boost eigen ROOT DD4hep
)

#--- Acts -------------------------------------------------------------------------------------------
LCGPackage_add(
    acts
    URL ${GenURL}/acts-${acts_native_version}.tar.gz
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
               -DACTS_BUILD_PLUGIN_IDENTIFICATION=ON
               -DACTS_BUILD_PLUGIN_DIGITIZATION=ON
               -DACTS_BUILD_PLUGIN_DD4HEP=ON
               -DACTS_BUILD_PLUGIN_TGEO=ON
               -DACTS_BUILD_UNITTESTS=OFF
               "-DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -I${XercesC_home}/include -I${eigen_home}/include/eigen3"
               ${Boost_extra_configuration}
    DEPENDS Boost eigen ROOT DD4hep
)

#--- RooUnfold ---------------------------------------------------------------------------------------
LCGPackage_add(
    RooUnfold
    URL ${GenURL}/RooUnfold-${RooUnfold_native_version}.tar.gz
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
               -DRooUnfoldTests=OFF
               -DRooUnfoldGenerateCMakeConfig=ON
    DEPENDS ROOT
)
