#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev3)

SET(LHCB_HEPMC 3)
LCG_remove_package(HepMC)
LCG_remove_package(Gaudi)
LCG_remove_package(cepgen)
LCG_remove_package(hepmcanalysis)

SET(LHCB_JSON_FILE https://gitlab.cern.ch/lhcb-core/rpm-recipes/-/raw/master/LHCBEXTERNALS/dev3lhcb.json)

include(heptools-lhcbsetup)

# Latest version in dev-generators. LHCB_7 not working with python3.11 or hepmc=3
LCG_external_package(lhapdf            6.2.3p1        ${MCGENPATH}/lhapdf author=6.2.3 usecxxstd=1 )
LCG_external_package(crmc              2.0.1p5        ${MCGENPATH}/crmc  author=2.0.1 hepmc=${LHCB_HEPMC}   )
LCG_external_package(yoda              2.0.0          ${MCGENPATH}/yoda  )
LCG_external_package(rivet             4.0.0          ${MCGENPATH}/rivet hepmc=${LHCB_HEPMC} author=4.0.0)
LCG_external_package(thepeg            2.2.3          ${MCGENPATH}/thepeg hepmc=${HEPMC_VERSION})
LCG_external_package(herwig3           7.2.3p2        ${MCGENPATH}/herwig++ thepeg=2.2.3 hepmc=${HEPMC_VERSION} author=7.2.3)
