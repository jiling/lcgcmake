if(APPLE)

  # keep this version of Qt5 because it is already available and
  # compiled once newer version has issues to build, or cannot be
  # built because SDK version too new
  LCG_external_package(Qt5               5.15.9                  qt5             )

  LCG_external_package(libuv      1.44.2)     # Needed by horovod
  LCG_external_package(appnope    0.1.2)

  LCG_remove_package(bear)
  LCG_remove_package(catboost)
  LCG_remove_package(cepgen)
  LCG_remove_package(cx_oracle)
  LCG_remove_package(Frontier_Client)
  LCG_remove_package(gdb)
  LCG_remove_package(grpc)   # needs patch for clang issue with std::result_of
  LCG_remove_package(GitCondDB)
  LCG_remove_package(heaptrack)
  LCG_remove_package(herwig3)
  LCG_remove_package(hls4ml)
  LCG_remove_package(kokkos)
  LCG_remove_package(pepper_kokkos)
  #LCG_remove_package(mpi4py)
  LCG_remove_package(octave)
  LCG_remove_package(octavekernel)
  LCG_remove_package(openloops)               # fails building on MacOS (compiler crash)
  LCG_remove_package(geneva)                  # depends on openloops
  #LCG_remove_package(openmpi)
  LCG_remove_package(oracle)
  LCG_remove_package(oracledb)
  LCG_remove_package(librdkafka)              # not needed for macos,requested by SWAN
  LCG_remove_package(confluent_kafka)              # not needed for macos,requested by SWAN
  LCG_remove_package(pango)
  LCG_remove_package(prctl)
  LCG_remove_package(pyroscope)
  LCG_remove_package(PyHEADTAIL)
  LCG_remove_package(qcachegrind)
  LCG_remove_package(R)                       # Clash between R and ROOT headers (case insensitive) 
  LCG_remove_package(rpy2)                    #   ...
  LCG_remove_package(rust)                    # installer not available as simple tar file, could be solved with a bit more effort
  LCG_remove_package(ruff)                    # needs rust
  LCG_remove_package(scikitimage)
  LCG_remove_package(sherpa)
  LCG_remove_package(sherpa-openmpi)
  LCG_remove_package(tensorflow_probability)
  LCG_remove_package(tensorflow)              # no binaries available for arm64, needs jax
  LCG_remove_package(tf_keras)                # missing tensorflow
  LCG_remove_package(transformers)            # missing tf_keras
  LCG_remove_package(sentence_transformers)   # missing transformers
  LCG_remove_package(tensorflow_estimator)    # missing tensorflow
  LCG_remove_package(tensorflow_model_optimization) # missing tensorflow
  LCG_remove_package(tensorflow_io_gcs_filesystem)  # missing tensorflow
  LCG_remove_package(tensorboard_data_server) # missing arm64 wheel
  LCG_remove_package(tensorboard)             # missing tensorboard_data_server
  LCG_remove_package(tensorboard_plugin_wit)  # missing tensorflow
  LCG_remove_package(tf2onnx)                 # missing tensorflow
  LCG_remove_package(keras_tuner)             # missing tensorboard
  LCG_remove_package(QKeras)                  # missing tensorflow_model_optimization
  LCG_remove_package(zfit)                    # missing tensorflow
  LCG_remove_package(zfit_physics)            # needs zfit

  LCG_remove_package(unigen)                  # lonk error
  LCG_remove_package(valgrind)
  LCG_remove_package(xgboost)

  LCG_remove_package(jaxlib)                  # missing for macos x86, only for pyhf
  LCG_remove_package(jax)                     # missing for macos x86, for pyhf and tensorflow
  LCG_remove_package(pyhf)                    # cannot have jax/jaxlib
  LCG_remove_package(cabinetry)               # needs pyhf
  LCG_remove_package(llvmlite)                # missing for macos x86
  LCG_remove_package(numba)                   # needs llvmlite
  LCG_remove_package(numba_stats)             # needs numba
  LCG_remove_package(qibojit)                 # needs numba
  LCG_remove_package(coffea)                  # needs numba
  LCG_remove_package(scikit_rf)               # needs numba
  LCG_remove_package(faiss)                   # needs newer cmake than on mac11

  LCG_remove_package(vbfnlo)                  # internal assertion of loaded (ld) 
  LCG_remove_package(cramjam)                 # No Rust on macos which is a cramjam dependency

  if(LCG_TARGET MATCHES mac1[23])
    LCG_remove_package(EDM4hep)               # cmake is too old for EDM4hep
  endif()

  if(LCG_TARGET MATCHES mac15)
    LCG_remove_package(Qt5)  # does not build in any version in macos15
    # dropped packages because they need Qt5
    LCG_remove_package(cppcheck)
    LCG_remove_package(jupyter)
    LCG_remove_package(jupyter_contrib_nbextensions)
    LCG_remove_package(pygraphics)
    LCG_remove_package(pyqt5 )
    LCG_remove_package(pyqt5_sip)
    LCG_remove_package(pyshtools)
    LCG_remove_package(pytools)
    LCG_remove_package(qtconsole)
    LCG_remove_package(qtpy)
    # switch to Qt6
    LCG_external_package(VTK               9.3.1           Qt=6 )
    set(Geant4_Qt 6)
  endif()

  # not compatible with this architecture
  LCG_remove_package(protobuf2)

  if(LCG_ARCH MATCHES arm64)
    LCG_remove_package(nlox)
    LCG_remove_package(ginac)
    LCG_remove_package(qqvvamp)
    LCG_remove_package(ggvvamp) 
    LCG_remove_package(gosam)
    LCG_remove_package(gosam_contrib)
    LCG_remove_package(madgraph5amc)
    LCG_remove_package(madx)
    LCG_remove_package(cpymad)
    LCG_remove_package(C50)

    LCG_remove_package(itk_core)                # installs a version of numpy, which gets relocated and produces a codesign error (Killed 9)
    LCG_remove_package(itk_io)                  # ...
    LCG_remove_package(itk_numerics)            # ...
    LCG_remove_package(itk_filtering)           # ...
    LCG_remove_package(itk_registration)        # ...
    LCG_remove_package(itk_segmentation)        # ...
    LCG_remove_package(itk)                     # ...
    LCG_remove_package(itk_meshtopolydata)      # does not exists binary
    LCG_remove_package(itkwidgets)              # ...

    LCG_remove_package(httpstan)                # no wheel for httpstan package, which is needed by pystan
    LCG_remove_package(pystan)                  # no wheel for httpstan package, which is needed by pystan
    LCG_remove_package(kubernetes_asyncio)      # ...
    LCG_remove_package(dask_kubernetes)         # ...  
    LCG_remove_package(onnxruntime)             # the wheel created is x86_64 only

  endif()
endif()
