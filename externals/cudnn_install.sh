#!/bin/bash

set -eux

CUDNN_PROJECT_DIR=$1
CUDNN_INSTALL_DIR=$2

# we can only distribute shared libraries, everything else has to be a link

# Link to header files
#
# here we go from the projects folder and create the header file link
mkdir -p ${CUDNN_INSTALL_DIR}/include/
for FILE in ${CUDNN_PROJECT_DIR}/include/*.h; do
    ln -v -s -f ${FILE} ${CUDNN_INSTALL_DIR}/include/$(basename $FILE)
done

# Replace static libraries with links
#
# here we take the static archives in intall directory, because we have to extract the shared libraries anyway
for FILE in ${CUDNN_INSTALL_DIR}/lib/*.a; do
    ln -v -s -f ${CUDNN_PROJECT_DIR}/lib/$(basename $FILE) $FILE
done

