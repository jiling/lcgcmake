#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev4)

# Other
LCG_external_package(bdsim                                 1.7.7)
LCG_external_package(bhlumi                         4.04linuxLHE)
LCG_external_package(fcalclusterer                         01.01)
LCG_external_package(generalbrokenlines                 02.02.01)
LCG_external_package(kkmcee                           2024.10.07)
LCG_external_package(opendatadetector                      v4.0.3 GIT=https://gitlab.cern.ch/acts/OpenDataDetector.git)
LCG_external_package(pybdsim                               3.6.1)
LCG_external_package(pymadx                                2.2.1)
LCG_external_package(pytransport                           2.0.2)
LCG_external_package(simsipm                               2.0.2)
LCG_external_package(whizard                               3.1.4)


# Key4hep
LCG_external_package(cldconfig                        2024.10.06)
LCG_external_package(k4clue                             01.00.06)
LCG_external_package(k4edm4hep2lcioconv                    00.09)
LCG_external_package(k4fwcore                           01.01.02)
LCG_external_package(k4generatorsconfig                    0.1.1)
LCG_external_package(k4geo                                 00.21)
LCG_external_package(k4marlinwrapper                       00.10)
LCG_external_package(k4_project_template                   0.5.0)
LCG_external_package(k4reco                                0.1.0)
LCG_external_package(k4rectracker                          0.3.0)
LCG_external_package(k4simdelphes                       00.07.03)

# iLCSoft
LCG_external_package(ced                                   01.10)
LCG_external_package(cedviewer                             01.20)
LCG_external_package(clicperformance                    02.05.01)
LCG_external_package(clupatra                           01.03.01)
LCG_external_package(conddbmysql                           0.9.7)
LCG_external_package(conformaltracking                     01.12)
LCG_external_package(ddkaltest                          01.07.01)
LCG_external_package(ddmarlinpandora                    00.12.02)
LCG_external_package(forwardtracking                    01.14.02)
LCG_external_package(garlic                                03.01)
LCG_external_package(gear                               01.09.04)
LCG_external_package(ilcutil                            01.07.03)
LCG_external_package(ildperformance                        01.12)
LCG_external_package(kaldet                             01.14.01)
LCG_external_package(kaltest                            02.05.02)
LCG_external_package(kitrack                            01.10.01)
LCG_external_package(kitrackmarlin                      01.13.02)
LCG_external_package(lccd                               01.05.03)
LCG_external_package(lcfiplus                           00.10.01)
LCG_external_package(lcfivertex                            00.09)
LCG_external_package(lctuple                               01.14)
LCG_external_package(marlin                             01.19.04)
LCG_external_package(marlindd4hep                       00.06.02)
LCG_external_package(marlinfastjet                      00.05.03)
LCG_external_package(marlinkinfit                       00.06.01)
LCG_external_package(marlinkinfitprocessors                00.05)
LCG_external_package(marlinmlflavortagging                 0.1.0)
LCG_external_package(marlinReco                         01.36.01)
LCG_external_package(marlintrk                          02.09.02)
LCG_external_package(marlintrkprocessors                02.12.06)
LCG_external_package(marlinutil                         01.18.01)
LCG_external_package(overlay                            00.23.01)
LCG_external_package(physsim                               00.05)
LCG_external_package(raida                                 01.11)

# PandoraPFA
LCG_external_package(larcontent                         04.11.02)
LCG_external_package(lccontent                          03.01.06)
LCG_external_package(pandoraanalysis                    02.00.01)
LCG_external_package(pandoramonitoring                  03.06.00)
LCG_external_package(pandorapfa                         04.11.02)
LCG_external_package(pandorasdk                         03.04.02)

# AIDASoft
LCG_external_package(aidatt                                00.10)

# HEP-FCC
LCG_external_package(dual_readout                          0.1.4)
LCG_external_package(fcc_config                            0.1.0)
LCG_external_package(fccdetectors                       0.1pre10)
LCG_external_package(fccsw                              1.0pre10)
LCG_external_package(k4gen                              0.1pre13)
LCG_external_package(k4simgeant4                      0.1.0pre15)

# Other packages thate require checks

# Check in dev-base for onnxruntime
if(${LCG_COMP}${LCG_COMPVERS} MATCHES "gcc1[1234]" )
  # A more recent version that what is in dev-base is required for ddfastshowerml
  LCG_external_package(openmpi                               5.0.6)
  LCG_external_package(ddfastshowerml                        0.1.0)
  LCG_external_package(k4reccalorimeter                 0.1.0pre16)
endif()


if(NOT ${LCG_OS} MATCHES mac)
  LCG_external_package(fccanalyses                          0.10.0) # Until https://github.com/HEP-FCC/FCCAnalyses/issues/417 is fixed
  LCG_external_package(freeglut                              3.6.0) # Taken from the system on Mac
  LCG_external_package(babayaga                           fcc1.0.0) # Does not build on MacOS
endif()
