#!/bin/bash

# Usage: ./find_deepest_existing_dir.sh <path>
# This script returns the deepest existing path in the given path

TARGET_DIR="$1"
CURRENT_DIR="$TARGET_DIR"

while [ ! -d "$CURRENT_DIR" ] && [ "$CURRENT_DIR" != '/' ]; do
    CURRENT_DIR=$(dirname "$CURRENT_DIR")
done
echo "$CURRENT_DIR"  # This is the only output to stdout
