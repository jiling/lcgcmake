#!/bin/bash

# run_dd4hep_example_tests.sh <local-prefix>  <platform>  <source-dir> <dd4hep_version>

PREFIX=$1
PLATFORM=$2
SOURCE_DIR=$3
VERSION=$4

# where is lcgenv?
if [ -z "$LCGENV" ]; then
  if command -v lcgenv &>/dev/null; then
     LCGENV=`command -v lcgenv`
  elif [ -x /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv ]; then
     LCGENV=/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv
  fi
fi

# how many cores?
CPUS=`grep -c ^processor /proc/cpuinfo`   

# set current directory and environment
echo "Source dir is ${SOURCE_DIR}"
cd ${SOURCE_DIR}/examples
rm -rf build
mkdir build
cd build

eval "`$LCGENV -p ${PREFIX} ${PLATFORM} DD4hep ${VERSION}`"
#env | sort | sed 's/:/:?     /g' | tr '?' '\n'

source ${DD4HEP__HOME}/bin/thisdd4hep.sh
cmake -G Ninja \
      -DBoost_NO_BOOST_CMAKE=ON \
      -DDD4HEP_USE_XERCESC=ON \
      -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD} \
      ..

ninja install

ctest -j${CPUS} --output-on-failure
