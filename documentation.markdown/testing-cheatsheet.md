1. Setup as usual
2. `make` packages that are to be tested
3.  Run `make rivet cmaketools`, `ctest  -R lhapdf6sets.download`, `ctest -R rivet-tests` to get things needed by tests
4. `ctest -vv -R <package>` to test <package>