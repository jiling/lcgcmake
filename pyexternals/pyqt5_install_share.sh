#!/bin/bash -x
#
# install *.sip files to share
#

SOURCE_DIR=$1
INSTALL_DIR=$2

mkdir -p ${INSTALL_DIR}/share/sip/PyQt5/


for d in ${SOURCE_DIR}/sip/**/; do
  set -- "$d/"*.sip
  if [ -e "$1" ]; then
      cp -rp -- "$d" ${INSTALL_DIR}/share/sip/PyQt5/
  fi
done

