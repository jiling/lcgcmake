#!/bin/bash -x
# Wait until today's nightly is available on stratum 1
# wait_for_cvmfs $VOLUME $REVISION

VOLUME=$1
REVISION=$2

for iterations in {1..720}
do
    if [ `uname -s` == Darwin ]; then
        current_rev=`xattr -p user.revision /cvmfs/$VOLUME`
        if [ $? -ne 0 ]; then
            # Probably the CVMFS volume is not mounted. Try to mount it...
            # we need to mount cvmfs-config before we can mount the other repositories
            # needed for CVMFS_HTTP_PROXY config variable
            echo "Mounting CVMFS volume config-cvmfs.cern.ch"
            sudo mount -t cvmfs cvmfs-config.cern.ch /Users/Shared/cvmfs/cvmfs-config.cern.ch || true
            echo "Mounting CVMFS volume $VOLUME"
            sudo mount -t cvmfs $VOLUME  /Users/Shared/cvmfs/$VOLUME
            current_rev=`xattr -p user.revision /cvmfs/$VOLUME`
        fi
        # make sure sft.cern.ch is also mounted
        # sft.cern.ch is also needed later for running the tests
        if [ $VOLUME != sft.cern.ch ]; then
            if ! xattr -p user.revision /cvmfs/sft.cern.ch ; then
                echo "Mounting CVMFS volume sft.cern.ch"
                sudo mount -t cvmfs sft.cern.ch /Users/Shared/cvmfs/sft.cern.ch || true
            fi
        fi
    else
        current_rev=$(attr -qg revision /cvmfs/$VOLUME/ 2>/dev/null)

        # If the first method fails, try the second method
        if [[ -z "$current_rev" ]]; then
            current_rev=$(attr -qg revision /var/spool/cvmfs/$VOLUME/rdonly/ 2>/dev/null)
        fi

    fi
    if [ "${current_rev}" -ge "${REVISION}" ]
    then
        echo "[INFO] Current revision [${current_rev}] is greater or equal than the required [${REVISION}]."
        break
    else
        if  [[ "${iterations}" == "720" ]]
        then
            echo "[ERROR] Abort after one hour of waiting."
            exit 1
        else
            # echo "[WARNING] Revision is not yet present on CVMFS stratum 1. Going to sleep for 5 seconds ..."
            sleep 5
        fi
    fi
done
exit 0
