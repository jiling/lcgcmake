#!/usr/bin/env python
# Add lcg python common module to PYTHONPATH
from __future__ import print_function
import sys, os
# sys.path.append("{0}/../../python".format(os.path.dirname(os.path.realpath(__file__))))
sys.path.append("{0}/python".format(os.path.dirname(os.path.realpath(__file__))))

from lcg.buildinfo import read_buildinfo_from_tarfile

template_dir = os.path.dirname(os.path.realpath(__file__)) + "/spec"

def generate_specfile(info, **argd):
    if "PLATFORM" in argd:
        argd["PLATFORM_U"] = argd["PLATFORM"].replace("-", "_") # Underscored platform name
        argd["PLATFORM_SHORT"] = '-'.join(argd["PLATFORM"].split("-")[:2]) # Only arch and system
        argd["PLATFORM_SHORT_U"] = '_'.join(argd["PLATFORM"].split("-")[:2]) # Only arch and system

    # Calculate revision number
    # Revision number from buildinfo
    rev = info.get("REVISION", 0)
    if rev.isdigit():
        info["REVISION"] = args.revision + int(rev) + 1 # The revision number in rpm starts from one and LCGCmake has None
    else:
        info["REVISION"] = args.revision + 1 # Revision in lcgcmake is not a number (backport package) - dont use rev
        
    info['DEPS'] = []
    for req in info['DEPENDSDICT']:
        info['DEPS'].append('Requires:   {NAME}_{VERSION}_{PLATFORM_U}'.format(VERSION=req['VERSION'], HASH=req['HASH'], NAME=req['NAME'], PLATFORM_U=argd["PLATFORM_SHORT_U"]))


    info['DEPS'] = "\n".join(info['DEPS'])

    template = open(os.path.join(template_dir, 'template.spec')).read()
    info.update(argd)
    return template.format(**info)

def create_specfile(source, platform, url=None, **options):
    buildinfo_dict = read_buildinfo_from_tarfile(source)[0]
    src = source
    if url:
        src=url+os.path.basename(source)
    return generate_specfile(buildinfo_dict, PLATFORM=platform, SOURCE=src, **options), "{0}-{1}".format(buildinfo_dict["NAME"], buildinfo_dict["VERSION"])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='LCG RPM creator', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--platform", type=str, help='Example: x86_64-centos7-gcc62-opt')
    parser.add_argument("--output", default="SPECS", help="SPEC files output dir")
    parser.add_argument("--url", default="http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases/", help="")
    # parser.add_argument("--create-setup", dest="setup", help="Create setup.{sh,csh} inside RPM", action="store_true")
    # parser.add_argument("--short-platform", dest="short_platform", help="Use only arch-os as platform name", action="store_true")
    parser.add_argument("--revision", default=0, help="RPM release  number (add to LCGCMake revision number)", type=int)
    parser.add_argument("-v","--verbose", action="store_true")
    parser.add_argument("tarballs", type=str, nargs="+") 

    args = parser.parse_args()

    for tar in args.tarballs:
        if args.verbose:
            print("Generate specfile for {0}".format(tar))
        spec, name = create_specfile(tar, args.platform, args.url, revision=args.revision)  # SETUP=args.setup, SHORT_PLATFORM=args.short_platform,
                
        with open("{0}/{1}.spec".format(args.output, name), "w") as f:
            f.write(spec)


