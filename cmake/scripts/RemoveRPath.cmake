# Strip the rpath from all .so files in the given dir
# Parameters:
#    INSTALL_DIR Installation prefix

cmake_policy(SET CMP0009 NEW) # FILE GLOB_RECURSE calls should not follow symlinks by default

file(GLOB_RECURSE sharedlibs ${INSTALL_DIR}/*.so ${INSTALL_DIR}/bin/*)
foreach(file ${sharedlibs})
  if(NOT APPLE)
    # Save the original permissions
    execute_process(COMMAND stat -c "%a" "${file}" OUTPUT_VARIABLE permissions)
    string(REGEX REPLACE "(\r?\n)+$" "" permissions "${permissions}")

    # Temporarily make the file writable
    execute_process(COMMAND chmod ugo+wx "${file}")

    # Get the current RPATH using patchelf
    execute_process(
      COMMAND patchelf --print-rpath "${file}"
      OUTPUT_VARIABLE current_rpath
      OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    # Only proceed if RPATH is not empty
    if(NOT "${current_rpath}" STREQUAL "")
        # Split RPATH into individual paths and filter out those without $ORIGIN
        string(REPLACE ":" ";" rpath_list "${current_rpath}")

        set(new_rpath "")
        foreach(rpath_entry ${rpath_list})
            if(rpath_entry MATCHES "\\$ORIGIN")
                if(new_rpath STREQUAL "")
                    set(new_rpath "${rpath_entry}")
                else()
                    set(new_rpath "${new_rpath}:${rpath_entry}")
                endif()
            endif()
        endforeach()

	# NOTE: We do not check the exit code of the patchelf command here because it is executed
        # on all files, not just ELF objects. patchelf will return a non-zero exit code if the file
        # is not an ELF object, which is expected in our current setup.
        # This script should be revised to ensure that the patchelf command is only
        # run on ELF objects, and we should then check the exit code to fail gracefully in case of an error.

	# If the new RPATH differs from the original, update it
        if(NOT new_rpath STREQUAL "${current_rpath}")
            execute_process(COMMAND patchelf --set-rpath "${new_rpath}" "${file}")
        endif()
    endif()

    # Restore the original permissions
    execute_process(COMMAND chmod ${permissions} "${file}")
  endif()
endforeach()
