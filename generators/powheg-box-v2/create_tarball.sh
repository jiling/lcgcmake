#!/bin/bash
VER=$1
REV=$2
SUF=""
[ $1 -eq 2 ] && SUF=-v2
rm -rf POWHEG2 2>/dev/null
./prepare.sh $1 $2 
for lb in Zj WZ Wgamma W2jet trijet HZ HJJ Z_ew-BMNNPV WW \
  W_ew-BMNNP VBF_H W HWJ HJ Z2jet ST_sch ST_tch ST_tch_4f ST_wtch_DR ST_wtch_DS \
  HW ggHZ ZZ Z Wj Wbb_dec vbf_wp_wp ttb_dec HZJ hvq gg_H_quark-mass-effects \
  DMS_tloop DMS DMGG DMV HW_smeft HZ_smeft VBF_HJJJ VBF_H_smeft VBF_W-Z \
  VBF_Wp_Wm VBF_Z WWanomal WZanomal W_smeft Wbb_nodec Wbbj Wp_Wp_J_J bbH dijet \
  dislepton-jet gg_H ttH ttb_NLO_dec; do ./prepare.sh $1 $2 $lb; done
tar -zcf powheg-box$SUF-r$REV.lhcb.rdynamic.tar.gz POWHEG$VER/
